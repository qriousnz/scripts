# Qrious utility scripts
```
curl -s "https://bitbucket.org/qriousnz/scripts/raw/d27133f96a6da695cf5c282b0b299de5a883baa8/ansible-pull.sh" > ansible-pull.sh && chmod 755 ansible-pull.sh && sh ansible-pull.sh playbooks/revera/springxd.yml
```

# VMWare snapshot API

```
usage: snapshot_tool.py [-h] [-s SERVER] [-u USERNAME] [-p PASSWORD]
                        [-pe PASSWORDENCRYPTED] -m VMNAME [-v] [-d]
                        [-l LOGFILE] [-V]
                        {list,create,delete,revert} ...

Manage VM snapshots. Create, Delete, List and Revert to Snapshot.

positional arguments:
  {list,create,delete,revert}
                        commands
    list                List Snapshot(s) of a given VM
    create              Create a Snapshot of a given VM
    delete              Delete a Snapshot of a given VM
    revert              Revert to a Snapshot of a given VM

optional arguments:
  -h, --help            show this help message and exit
  -s SERVER, --server SERVER
                        The vCenter or ESXi server to connect to
  -u USERNAME, --user USERNAME
                        The username with which to connect to the server
  -p PASSWORD, --password PASSWORD
                        The password in plain text with which to connect to
                        the host. If not specified, the user is prompted at
                        runtime for a password.
  -pe PASSWORDENCRYPTED, --password-encrypted PASSWORDENCRYPTED
                        The password encrypted using Base64 with which to
                        connect to the host.
  -m VMNAME, --vm VMNAME
                        The virtual machine (VM) to manage snapshots
  -v, --verbose         Enable verbose output
  -d, --debug           Enable debug output
  -l LOGFILE, --log-file LOGFILE
                        File to log to (default = stdout)
  -V, --version         show program's version number and exit
```


## Example

### LIST
e.g. list snapshots for VM "CDIC"

`python snapshot_tool.py -m CDIC list`

### CREATE
e.g. create a snapshot named "snap_201610251030" for VM "CDIC" with description "Testing api"

`python snapshot_tool.py -m CDIC create -sn snap_201610251030 -sd "Testing api"`

### DELETE
e.g. delete a snapshot named "snap_201610251030" for VM "CDIC"

`python snapshot_tool.py -m CDIC -d delete -sn snap_201610251030`

### REVERT
e.g. revert to the snapshot named "snap_201610251030" for VM "CDIC"

`python snapshot_tool.py -m CDIC -d revert -sn snap_201610251030`


