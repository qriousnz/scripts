#!/bin/bash

script_dir="/Users/davidlu/bitbucket_tools/scripts/vmware"
current_day=`date +%Y-%m-%d`
#del_day=`date +%Y-%m-%d -d "3 days ago"`
del_day=`gdate +%Y-%m-%d -d "3 days ago"`
snapshot_time=`date +%Y%m%d%H%M`
vmname_conf="${script_dir}/vmname_snapshot.conf"
#vmname_conf="${script_dir}/vmname_snapshot_test.conf"
snapshot_tool="${script_dir}/snapshot_tool.py"

#vmname=$1
snapshot_name="snap_${snapshot_time}"
desc="auto snapshot"
LOG="${script_dir}/vmware.log"

cd ${script_dir}

while read vmname
do
    # create new snapshot
#    python ${snapshot_tool} -m "${vmname}" list
    python ${snapshot_tool} -m "${vmname}" -l "${LOG}" create -sn "${snapshot_name}" -sd "${desc}"
#    sleep 3

    # list snapshot 3 days ago
    #after_mess 2016-10-20
    #after_recovery 2016-10-20
    vmname_del=`python ${snapshot_tool} -m "${vmname}" list 2>&1 | awk -F';' '{print $2, $4}' | awk -v var="${del_day}" '{if($4 < var) print $2}' | head -n 1`

    # delete
    if [[ ${vmname_del} != "" ]]
    then
        echo python ${snapshot_tool} -m "${vmname}" -d -l "${LOG}" delete -sn "${vmname_del}"
    else
        echo "${vmname}: nothing to be deleted"
    fi
done < ${vmname_conf}
