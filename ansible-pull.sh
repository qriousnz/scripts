#!/bin/bash

export http_proxy=http://10.101.32.101:443
export https_proxy=https://10.101.32.101:443

USER=$(whoami)
USERDIR="/home/$USER/.ansible"
mkdir -p $USERDIR
TEMPDIR=$(mktemp -d ansible-playbooks.XXXXXXXX)

while getopts "h:u:p:d:" flag; do
case "$flag" in
    p) GITPASS=$OPTARG;;
    u) GITUSER=$OPTARG;;
esac
done
PLAYBOOK=${@:$OPTIND:1}

if [ "" = "$GITUSER" ] || [ "" = "$GITPASS" ]
then
  read -p "Git Username: " GITUSER
  read -s -p "Git Password: " GITPASS
fi

GITURL="https://$GITUSER:$GITPASS@bitbucket.org/qriousnz/ansible-playbook.git"
printf "\n$0 -p *** -u $GITUSER ansible-playbook $PLAYBOOK ...\n"

sudo yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo yum install -y git ansible
cd $USERDIR
git clone --depth 1 -b staging $GITURL $TEMPDIR
cd $TEMPDIR

ansible-playbook -i "localhost," -c local $PLAYBOOK -vv

cd ..
#rm -fr $TEMPDIR
printf "Please remove $TEMPDIR after installation\n"
exit 0