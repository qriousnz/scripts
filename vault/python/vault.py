#!/usr/bin/python
import sys
import datetime
import yaml
import time
import os
import logging

https_proxy='10.101.32.101:443'

def get_keys(config):
    try:
        stream = open(config, "r")
        docs = yaml.load(stream)
        return docs['vault']['access_key'], docs['vault']['secret_key']
    except yaml.YAMLError as exc:
        logging.error('Credential Keys are not right!')
        print(exc)

def get_path(catalog, filename):
    now = datetime.datetime.now()
    date_year = str(now.year)
    date_month = str('%02d' % now.month)
    date_day = str('%02d' % now.day)
    path = catalog + "/" + date_year + "/" + date_month + "/" + date_day + "/" + filename
    logging.info("Upload File Path: " + path)
    return path

def get_date():
    utc_now = datetime.datetime.utcnow()
    date = utc_now.strftime("%a, %d %b %Y %X +0000")
    logging.info("Generate UTC Time Header: "+ date)
    return str(date)

def curl(url, filename, date, access_key, sig, content_type, md5):
    command = 'curl -s -x '+ https_proxy  + ' -T ' +  filename + " " + url + \
    ' -H "Date: ' + date + '"' \
    ' -H "Authorization: AWS ' + access_key + ":" + sig + '"' \
    ' -H "Content-Type: ' + content_type + '"'\
    ' -H "Content-MD5: ' + md5 + '"'
    logging.info("Curl Command: " +command)
    out = os.popen(command).read()
    logging.info("HTTP_RETURN: " +out)
    return out

def main():
    if len(sys.argv) != 4:
        print "Usage: vault-put bucket_name catalog src_file"
        print "eg. vault-put msd-staging-log  logstash beats-2016-04-01.gz"
        print "file path would be: https://vault.revera.co.nz/msd-staging-log/logstash/2016/04/01/beats-2016-04-01.gz"
        exit()
    else:
        bucket = sys.argv[1]
        catalog = sys.argv[2]
        filename = sys.argv[3]
    logging.basicConfig(filename='/var/log/vault.log', level=logging.INFO)
    logging.info('Started')
    content_type = "application/octet-stream"
    access_key, secret_key = get_keys("/etc/vault.yml")
    date = get_date()
    path = get_path(catalog, filename)
    md5 = os.popen("openssl md5 -binary < " + filename + " | base64").read().replace('\n','')
    Message = "PUT\n" + md5 + "\n" + content_type + "\n" + date + "\n/" + bucket + "/" + path
    #print(Message)
    signature = os.popen('printf "' + Message  + '" | openssl sha1 -binary -hmac "' + secret_key +  '" | base64').read().replace('\n','')
    url = "https://vault.revera.co.nz/" + bucket + "/" + path

    curl(url, filename, date, access_key, signature, content_type, md5)

if __name__ == '__main__':
    main()